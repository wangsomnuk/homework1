package day1;

public class lab3 {

	public static void main(String[] args) {
		int i = 20;
		float numA = 7.7f;
		float numB = 7f;
		char charA = 'a';
		char charB = 'b';

		
		System.out.println("round +1 = "+ i++);
		System.out.println("round +2 = "+ i++);
		System.out.println("round +3 = "+ i++);
		System.out.println("round +4 = "+ i++);
		System.out.println("round +5 = "+ i++);
		
		System.out.println("round -1 = "+ --i);
		System.out.println("round -2 = "+ --i);
		System.out.println("round -3 = "+ --i);
		System.out.println("round -4 = "+ --i);
		System.out.println("round -5 = "+ --i);
		

		
		if ((numA == 7.7f) && (numB == 7f))
			System.out.println("numA and numB is TRUE");
		
		if ((charA == 't') || (charB == 'b'))
			System.out.println("charA or charB is TRUE");
	}
}